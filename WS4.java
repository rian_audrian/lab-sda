import java.util.*;
import java.io.*;

public class WS4 {
	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		int n = Integer.parseInt(reader.readLine());
		
		String[] titles = new String[n];
		for (int i = 0; i < n; i++) {
			String title = reader.readLine();
			titles[i] = title;
		}
		
		Tree tree = new Tree();
		tree.root = tree.construct(titles, tree.root, 0);
		
		tree.postOrderRight();
	}
}

class Tree {
	Node root;
	// Do something
	
	Tree() {
		this.root = null;
	}
	
	Node construct(String[] titles, Node root, int i) {
		if (i < titles.length) {
			Node temp = new Node(titles[i]);
			root = temp;
			
			root.left = construct(titles, root, 2*i+1);
			root.right = construct(titles, root, 2*i+2);
			
			return root;
		} else {
			return null;
		}
	}
	
	void postOrderRight() {
		helperPostOrder(this.root);
	}
	
	void helperPostOrder(Node now) {
		if (now.right != null)
			helperPostOrder(now.right);
		
		if (now.left != null)
			helperPostOrder(now.left);
		
		System.out.println(now.val);
	}
}

class Node {
	Node left, right;
	String val;
	
	Node() {
		this.left = this.right = null;
		this.val = null;
	}
	
	Node(String val) {
		this.left = this.right = null;
		this.val = val;
	}
	
	public String toString() {
		return val;
	}
}

//TIDAK SEMUDAH ITU PULGOSOOOOOOOOOOOOO