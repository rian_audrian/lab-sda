import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Set;
import java.util.HashSet;

//Huge thanks to Anthony Dewa for the help

public class SortByNonPrimeFactors {
    public static void main(String args[]) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int N = Integer.parseInt(reader.readLine());
        Number[] numbers = new Number[N];

        for (int i = 0; i < N; i++) {
            numbers[i] = new Number(Integer.parseInt(reader.readLine()));
        }

        bubbleSort(numbers);
        printHasilSorting(numbers);
    }

    static private void printHasilSorting(Number[] arr) {
        int n = arr.length;
        for (int i = 0; i < n; i++) {
            if (i != 0)
                System.out.print(' ');
            System.out.print(arr[i].num);
        }
        System.out.println();
    }

    //Based on slide sda approach
    static private void bubbleSort(Number[] arr) {
        int N = arr.length;
        Number temp;
        for (int i = 0; i < N; i++) {
            for (int j = 1; j < N-i; j++) {
                if (arr[j-1].compareTo(arr[j]) > 0){
                    temp = arr[j-1];
                    arr[j-1] = arr[j];
                    arr[j] = temp;
                }
            }
        }
    }
}

class Number implements Comparable<Number> {
    // @TODO: lengkapi class dengan instance variable, constructor, dan method yang sesuai dan menurut anda diperlukan
    int num;
    Set<Integer> factors = new HashSet<Integer>();
    int npf = 0;

    public Number(int num) {
        this.num = num;
        findFactors();
        findNonPrimeFactors();
    }

    int findNonPrimeFactors() {
         // @TODO: lengkapi method ini untuk mencari faktor non prima dari sebuah bilangan
        

        for (Integer f: this.factors) {
            //System.out.println(f + " is prime: " + isPrime(f));
            if (!isPrime(f)) npf += 1;
        }

        //System.out.println(npf);
        this.npf = npf;
        
        return this.npf;
    }

    Set<Integer> findFactors() {
        this.factors.add(num);

        for (int i = 1; i <= this.num / 2; i++) {
            if (this.num % i == 0) this.factors.add(i);
        }

        //System.out.println(this.factors);
        return this.factors;
    }

    //Based on Dzaky Abdi's suggestion
    boolean isPrime(int i) {
        if (i <= 1) return false;

        for (int j = 2; j < i; j++) {
            if (i % j == 0) return false;
        }

        return true;
    }

    @Override
    public int compareTo(Number other) {
        // @TODO: lengkapi method untuk sorting sesuai dengan spesifikasi soal
        if (this.npf > other.npf) return 1;
        if (this.npf == other.npf) {
            if (this.num > other.num) return 1;
            else {
                return 0;
            }
        } else {
            return -1;
        }
    }
}