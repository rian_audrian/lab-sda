import java.util.Arrays;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.List;
/**
 * TP3 SDA - Muhammad Audrian 1706043790
 * Acknowledgements: Michael Christopher Sudirman (template), Visualgo, Geeks4Geeks, 
 * MightyPork (Tree Printer), Ariq Naufal Satria, Dhipta Raditya, Reynaldo Wijaya H, Gagah Pangeran R
 */

public class SDA1819T3printer {
    static void tester() {
        AVLTree tree = new AVLTree();
        TreePrinter tp = new TreePrinter();
        tree.root = tree.gabung("A");
        tp.print(tree.root);
        tree.root = tree.gabung("C");
        tree.root = tree.gabung("B");
        tree.root = tree.gabung("D");
        tree.root = tree.gabung("E");
        tree.root = tree.gabung("F");
        tree.root = tree.gabung("G");
        //System.out.println(tree.panjangTali("A", "B"));
        tp.print(tree.root);
        tree.print(tree.root);
    }
    public static void main (String[] args){
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            /*AVLTree tree = new AVLTree();
            int count = Integer.parseInt(reader.readLine());
            System.out.println(count);

            for (int i = 0; i < count; i++) {
                System.out.println(i); //DEBUG
                String[] input = reader.readLine().split(" ");
                System.out.println(input[0]);
                if (input[0] == "GABUNG") {
                    tree.root = tree.gabung(input[1]);
                } else if (input[0] == "GABUNG" && input[2] != null) {
                    tree.root = tree.gabung(input[1], Integer.parseInt(input[2]));
                } else if (input[0] == "PARTNER") {
                    tree.partner(input[1]);
                } else if (input[0] == "MENYERAH") {
                    tree.root = tree.menyerah(input[1]);
                } else if (input[0] == "CEDERA") {
                    tree.root = tree.menyerah(input[2]);
                } else if (input[0] == "PRINT") {
                    tree.print(tree.root);
                } else if (input[0] == "MUSIBAH") {
                    //tree.root = tree.musibah();
                } else if (input[0] == "PANJANG-TALI") {
                    tree.panjangTali(tree.get(input[1]), tree.get(input[2]));
                } else if (input[0] == "GANTUNGAN") {
                    ArrayList<Node> nodes = new ArrayList<>();
                    int size = Integer.parseInt(input[1]);

                    for (int j = 0; j < size; j++) {
                        nodes.add(tree.get(input[input.length - j]));
                    }

                    tree.gantungan(nodes);
                }
            }*/
           tester();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class Node implements TreePrinter.PrintableNode{
    Node left;
    Node right;
    String data;
    int height;
    public Node(String data){
        this.data = data;
    }
    //Data compareTo
    int compareTo(Node compared){
        if (this.data.compareTo(compared.data) == 0) return 0;
        else if (this.data.compareTo(compared.data) == 1) return 1;
        else return -1;
    }

    public Node getLeft() { return this.left; }

    public Node getRight() { return this.right; }

    public String getText() { return this.data; }
}

class AVLTree {
    //Standard AVL Tree stuff
    Node root;
    
    public AVLTree() {
        this.root = null;
    }

    public AVLTree(Node root) {
        this.root = root;
    }
    //End stuff

    //HELPER TO CALCULATE HEIGHT OF A GIVEN NODE
    int height(Node node) {
        if (node == null) return 0;
        System.out.println(node.data + ": Determining height");
        return node.height;
    }

    //HELPER TO CALCULATE BALANCE FACTOR
    int balanceFactor(Node node) {
        if (node == null) return 0;
        return height(node.right) - height(node.left);
    }

    int depth(String name) {
        return depth(this.root, name);
    }
    
    int depth(Node root, String name) {
        if (name.compareTo(root.data) > 0) { //GO RIGHT
            return 1 + depth(root.right, name);
        } else if (name.compareTo(root.data) < 0) { //GO LEFT
            return 1 + depth(root.left, name);
        } else return 0; //THIS IS THE NODE WE'RE LOOKING FOR
    }

    Node get(String name) {
        return get(this.root, name);
    }

    Node get(Node root, String name) {
        if (root == null) return null;

        if (name.compareTo(root.data) > 0) { //GO RIGHT
            return get(root.right, name);
        } else if (name.compareTo(root.data) < 0) { //GO LEFT
            return get(root.left, name);
        } else return root; //THIS IS THE NODE WE'RE LOOKING FOR
    }

    //HELPER TO FIND LOWEST COMMON ANCESTOR
    Node lowestCommonAncestor(Node root, Node one, Node two) {
        //BASE CASE
        if (root == null) return null;

        if (one == this.root || two == this.root) return this.root;

        if (one.compareTo(root) > 0 && two.compareTo(root) > 0) {
            //ONE AND TWO IS LARGER THAN ROOT, SO GO RIGHT
            return lowestCommonAncestor(root.right, one, two);
        } else if (one.compareTo(root) < 0 && two.compareTo(root) < 0) {
            //ONE AND TWO IS SMALLER THAN ROOT, SO GO LEFT
            return lowestCommonAncestor(root.left, one, two);
        }

        return root; //THIS IS THE LCA
    }

    Node findMax(Node root) {
        return (root.right != null) ? findMax(root.right) : root;
    }

    Node findMin(Node root) {
        return (root.left != null) ? findMin(root.left) : root;
    }

    Node rightRotate(Node node) {
        if (node == null) return null;

        Node pivot = node.left;
        //Node subtree = pivot.left;
        
        //System.out.println("Pivot " + pivot.data); 
        //System.out.println("Subtree " + subtree);

        node.left = pivot.right;
        pivot.right = node;

        node.height = Math.max(height(node.left), height(node.right)) + 1;
        pivot.height = Math.max(height(pivot.left), height(pivot.right)) + 1;

        return pivot;
    }

    Node leftRotate(Node node) {
        if (node == null) return null;

        Node pivot = node.right;
        //Node subtree = pivot.left;

        //System.out.println("Pivot " + pivot.data); 
        //System.out.println("Subtree " + subtree);

        node.right = pivot.left;
        pivot.left = node;

        node.height = Math.max(height(node.left), height(node.right)) + 1;
        pivot.height = Math.max(height(pivot.left), height(pivot.right)) + 1;

        return pivot;
    }

    //ACTUAL FUNCTION THAT'S CALLED
    Node gabung(String data) {
        return gabung(this.root, data);
    }

    Node rebalance(Node root) {
        System.out.println(root.data + " balance factor " + balanceFactor(root));

        if (balanceFactor(root) > 1) {
            if(balanceFactor(root.right) < 0) root.right = leftRotate(root.right);
            return leftRotate(root);
        }

        if (balanceFactor(root) < -1) {
            if(balanceFactor(root.left) > 0) root.left = rightRotate(root.left);
            return rightRotate(root);
        }

        return root;
    }

    //HELPER FUNCTION - STANDARD AVL INSERT
    Node gabung(Node root, String data) {
        if (root == null) {
            root = new Node(data);
            return root;
        }
        // System.out.println(root.data);
        if (data.compareTo(root.data) < 0) {
            root.left = gabung(root.left, data);
        }
        if (data.compareTo(root.data) > 0) {
            root.right = gabung(root.right, data);
        }

        root.height = Math.max(height(root.left), height(root.right)) + 1;
        System.out.println(root.data + " height " + root.height);

        System.out.println(root.data + " Rebalance");
        return rebalance(root);
        //System.out.println(root.height);
    }

    //Check criterias before inserting
    Node gabung(String data, int count){
        if (isAbleToJoin(this.root, data, count)){
            return gabung(this.root, data);
        }
        return null;
    }

    //Check criteria
    //Traverse the tree in reverse in order if going left
    //Traverse the tree in 'in order' if going right
    //if count reaches 1, check if Node can be placed
    //TODO read up on criterias of placing Nodes in AVL Tree.
    //Return true if can, return false if cannot.
    // If at any point when traversing the method finds null
    // Place node there 
    boolean isAbleToJoin(Node root, String data, int distance) {
        return false;
    }

    //Returns if node can be placed as a left child node or the right child node of that node.
    //Node can be placed if left child node null or left child node can be child of node that want to be placed(if left)
    // or right child node null or left child node can be child of node that want to be placed (if right)
    // TODO
    boolean isAbleToBePlacedThere(Node node){
        return false;
    }

    //returns the data inside node of either the left or the right
    //If exist right or left return the data inside node in right or left.
    String partner(String query) {
        return partner(this.root, query);
    }

    String partner(Node root, String query) {
       if (root == null) return "TIDAK ADA";

       if (query.compareTo(root.data) > 0) {
           return partner(root.right, query);
       } else if (query.compareTo(root.data) < 0) {
           return partner(root.left, query);
       } else {
           if (root.getLeft().data == query || root.getRight().data == query) {
               if (root.getLeft().data == query && root.getRight() != null) return root.getRight().data;
               if (root.getRight().data == query && root.getLeft() != null) return root.getLeft().data;
           }    
       }
       return "TIDAK ADA";
    }

    //Handles two cases of the Standard AVL Tree delete
    Node menyerah(String quitter){
        return delete(this.root, quitter, true);
    }

    //Handles two cases of the Standard AVL Tree delete
    Node cedera(String wounded){
        return delete(this.root, wounded, false);
    }

    //Modified Standard AVL Tree delete
    //with custom helper functions menyerah() and delete(), each handling two separate cases
    Node delete(Node root, String deleted, boolean quitting) {
        //HANDLE BASE CASE
        if (root == null) return null;

        if (deleted.compareTo(root.data) < 0) { //GO LEFT
            //System.out.println(deleted + ": " + root.data + ": Going left");
            root.left = delete(root.left, deleted, quitting);
        } else if (deleted.compareTo(root.data) > 0) { //GO RIGHT
            //System.out.println(deleted + ": " + root.data + ": Going right");
            root.right = delete(root.right, deleted, quitting);
        } else { 
            //System.out.println("found " + root.data);
            if (quitting) {
                //System.out.println("quitting");
                if (root.left != null && root.right != null) {
                    Node temp = findMax(root.left);
                    root.data = temp.data;
                    root.left = delete(root.left, temp.data, true);
                } else if (root.left != null || root.right != null) {
                    Node temp = (root.left != null) ? root.left : root.right;
                    root.data = temp.data;
                    if (root.left != null) root.left = null;
                    else root.right = null;
                } else {
                    root = null;
                }
            } else {
                //System.out.println("injured");
                if (root.left != null && root.right != null) {
                    Node pre = findMax(root.left);
                    Node suc = findMin(root.right);

                    Node temp = null;
                    if (height(pre) < height(suc)) {
                        temp = pre;
                    } else {
                        temp = suc;
                    }
                    root.data = temp.data;
                    if (temp.data == pre.data) root.left = delete(root.left, temp.data, false);
                    else root.right = delete(root.right, temp.data, false);
                } else if (root.left != null || root.right != null) {
                    Node temp = (root.left != null) ? root.left : root.right;
                    root.data = temp.data;
                    if (root.left != null) root.left = null;
                    else root.right = null;
                } else {
                    root = null;
                }
            }
        }
        if (root == null) return root;
        return rebalance(root);
    }

    //Prints the tree in pre order then post order
    void print(Node root){
        if (root == null) return;
        printPreOrder(root);
        System.out.println();
        printPostOrder(root);
    }

    //Prints the AVL Tree in pre order;
    //Root, left, right
    void printPreOrder(Node root){
        printHelper(root);
        if (root.left != null) printPreOrder(root.left);
        if (root.right != null) printPreOrder(root.right);
    }

    //Prints the AVL Tree in post order;
    //Left, Right, Root
    void printPostOrder(Node root){
        if (root.left != null) printPostOrder(root.left);
        printHelper(root);
        if (root.right != null) printPostOrder(root.right);
    }

    void printHelper(Node root) {
        if (root == null) return;
        System.out.print(root.data + ";");
    }

    //Prints the deleted tree in order
    //Left, root, right
    void musibah(){ //EVERY LEAF DIES
        musibahHelper(this.root);
    }

    //Helper function for musibah()
    //Finds every leaf node, prints its data then deletes it.
    void musibahHelper(Node root) {
        if (root == null) return;
    }

    int panjangTali(String n1, String n2) {
        return panjangTali(this.get(n1), this.get(n2));
    }
    //Find distance between two nodes of a binary tree
    int panjangTali(Node one, Node two) {
        Node lca = lowestCommonAncestor(this.root, one, two);
        return panjangTaliHelper(this.root, one) + panjangTaliHelper(this.root, two) + 2 * panjangTaliHelper(this.root, lca);
    }

    //Distance between root and node
    int panjangTaliHelper(Node root, Node one) {
        int count = 0;
        if (root == null || one == null) return Integer.MIN_VALUE;

        if (one.compareTo(root) < 0) {
            panjangTaliHelper(root.left, one);
            count++;
        }
        if (one.compareTo(root) > 0) {
            panjangTaliHelper(root.right, one);
            count++;
        }

        return count;
    }

    //Lowest common ancestor for all given nodes in a list.
    String gantungan(ArrayList<Node> nodes) {
        return "";
    }
}

/**
 * Binary tree printer
 *
 * @author MightyPork
 */
class TreePrinter
{
    /** Node that can be printed */
    public interface PrintableNode
    {
        /** Get left child */
        PrintableNode getLeft();
 
 
        /** Get right child */
        PrintableNode getRight();
 
 
        /** Get text to be printed */
        String getText();
    }
 
 
    /**
     * Print a tree
     *
     * @param root
     *            tree root node
     */
    public static void print(PrintableNode root)
    {
        List<List<String>> lines = new ArrayList<List<String>>();
 
        List<PrintableNode> level = new ArrayList<PrintableNode>();
        List<PrintableNode> next = new ArrayList<PrintableNode>();
 
        level.add(root);
        int nn = 1;
 
        int widest = 0;
 
        while (nn != 0) {
            List<String> line = new ArrayList<String>();
 
            nn = 0;
 
            for (PrintableNode n : level) {
                if (n == null) {
                    line.add(null);
 
                    next.add(null);
                    next.add(null);
                } else {
                    String aa = n.getText();
                    line.add(aa);
                    if (aa.length() > widest) widest = aa.length();
 
                    next.add(n.getLeft());
                    next.add(n.getRight());
 
                    if (n.getLeft() != null) nn++;
                    if (n.getRight() != null) nn++;
                }
            }
 
            if (widest % 2 == 1) widest++;
 
            lines.add(line);
 
            List<PrintableNode> tmp = level;
            level = next;
            next = tmp;
            next.clear();
        }
 
        int perpiece = lines.get(lines.size() - 1).size() * (widest + 4);
        for (int i = 0; i < lines.size(); i++) {
            List<String> line = lines.get(i);
            int hpw = (int) Math.floor(perpiece / 2f) - 1;
 
            if (i > 0) {
                for (int j = 0; j < line.size(); j++) {
 
                    // split node
                    char c = ' ';
                    if (j % 2 == 1) {
                        if (line.get(j - 1) != null) {
                            c = (line.get(j) != null) ? '┴' : '┘';
                        } else {
                            if (j < line.size() && line.get(j) != null) c = '└';
                        }
                    }
                    System.out.print(c);
 
                    // lines and spaces
                    if (line.get(j) == null) {
                        for (int k = 0; k < perpiece - 1; k++) {
                            System.out.print(" ");
                        }
                    } else {
 
                        for (int k = 0; k < hpw; k++) {
                            System.out.print(j % 2 == 0 ? " " : "─");
                        }
                        System.out.print(j % 2 == 0 ? "┌" : "┐");
                        for (int k = 0; k < hpw; k++) {
                            System.out.print(j % 2 == 0 ? "─" : " ");
                        }
                    }
                }
                System.out.println();
            }
 
            // print line of numbers
            for (int j = 0; j < line.size(); j++) {
 
                String f = line.get(j);
                if (f == null) f = "";
                int gap1 = (int) Math.ceil(perpiece / 2f - f.length() / 2f);
                int gap2 = (int) Math.floor(perpiece / 2f - f.length() / 2f);
 
                // a number
                for (int k = 0; k < gap1; k++) {
                    System.out.print(" ");
                }
                System.out.print(f);
                for (int k = 0; k < gap2; k++) {
                    System.out.print(" ");
                }
            }
            System.out.println();
 
            perpiece /= 2;
        }
    }
}