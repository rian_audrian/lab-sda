import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.ArrayList;

/** 
 * TP 4 SDA 
 * Muhammad Audrian A. P. 1706043790
 * Acknowledgements: Muhammad Indra R, Dhipta Raditya
 */


public class SDA1819T4 {

    static final int BASE = 29;

    static int[] powers;

    //static HashMap hashmap;
    static String str;

    //Precompute powers array
    static void precomputeBase() {
        powers[0] = 1;
        for (int i = 1; i < 1000; i++) {
            powers[i] = powers[i-1] * BASE;
        }
    }

    static int hash(String string) {
        int strlen = string.length();
        int result = 0;

        char[] splitStr = string.toCharArray();
        for (int i = 0; i < strlen; i++) {
            result += (int) splitStr[i] * powers[strlen - 1 - i];
        }

        return result;
    }

    //Soal tipe 1 using Rabin Karp algorithm
    static int solve1(String string, String substring) {
        int count = 0;

        int strlen = string.length();
        int sublen = substring.length();
        
        //Calculate substring hash value
        int subHash = hash(substring);

        //Calculate initial window hash value
        String window = string.substring(0, sublen);
        int curHash = hash(window);

        if (curHash == subHash) count++;

        //Loop through the string
        for (int i = 0; i < strlen - sublen; i++) {
            curHash -= (int) string.charAt(i) * powers[sublen - 1];
            curHash *= BASE;
            curHash += (int) string.charAt(i + sublen);

            if (curHash == subHash) count++;
        }

        return count;
    }

    
    //Soal tipe 2
    static void solve2() {

    }

    static void tester() {
       String test = "ccaccbccabbcbbccc";

       powers = new int[1000];
       precomputeBase();

       String testsub = "cc";

       System.out.println(solve1(test, testsub));
    }
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        try {
            String str = reader.readLine();
            
            int times = Integer.parseInt(reader.readLine());

            //Precompute powers array
            powers = new int[1000];
            precomputeBase();

            for (int i = 0; i < times; i++) {
                
                String[] input = reader.readLine().split(" ");

                if (input[0].equals("0")) { //SOAL TIPE 0
                    //System.out.println(input[1]);
                    System.out.println(solve1(str, input[1]));

                } else if (input[0].equals("1")) { //SOAL TIPE 1
                    solve2();

                } else {
                    continue; 
                }
            }

            //tester(); //DEBUG

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}